---
date: 2019-02-03
title: 'Kebiasaan Mencontek'
template: post
thumbnail: '../thumbnails/writing.png'
slug: mencontek
categories:
  - Personal
tags:
  - mencontek
  - cheating
---

Kalau teman-teman pernah bersekolah serta kuliah mungkin teman-teman adalah orang yang suka mencontek ketika ujian atau malah menjadi orang yang resah ketika melihat orang lain mencontek. 

Udah bukan suatu keheranan lagi sih melihat yang seperti itu, bukan hal baru lah intinya. Tapi kalau dipikir-pikir banyak juga faktor yang menyebabkan mereka mencontek. Intinya mah ada sebab dan akibat-nya. 

Mungkin mereka terpaksa mencontek karena soalnya susah atau mungkin kemudahan untuk mencontek. ya mudah banget sih tinggal ambil buku atau hp terus tutupi pake lembar jawaban atau lembar soal. 

Tapi ada hal yang jauh lebih baik dilakukan daripada melakukan hal itu, yaitu menjawab soal tersebut dengan sesuai kemampuan. 

Bukan berarti ketika penulis membuat artikel ini, penulis tidak pernah mencontek, tapi kita coba belajar suatu hal dari peristiwa ini. yang penulis tahu "Dunia Kerja" itu bebas, kita memiliki leluasa untuk semua hal yang mau kita pelajari dan kita lakukan. Tidak seperti dunia sekolah dan kuliah yang kaku dan terlihat deprecated. Di dunia kerja ketika kita bingung, mentok dan kehilangan inspirasi, kita bisa membuka internet untuk mencari sumber inspirasi. semudah itu. 

Pengalamanku ketika berkuliah yaitu mengerjakan tugas dari dosen dan teman-temanku dengan mudahnya bisa meminta softcopy dari yang sudah ku kerjakan. ya gimana ya, susah sih mentalnya udah pengennya yang mudah aja tinggal copy paste. pernah suatu ketika ada tugas dari dosen dan aku tak mengerjakan sampai deadline minus beberapa jam. Dan ternyata mengejutkan nya belum ada teman-temanku yang mengerjakan nya sama sekali, ya sama aja sih hitungan nya aku aku juga yang ngerjain terus mereka bisa minta softcopy nya. Ga mungkin dong ga ku kasih, nanti chaos lagi dan terlihat egois. 

Sekarang aku sudah tidak berkuliah di kampus tersebut. Banyak alasan dan banyak cerita yang mungkin bisa ditulis tentang "why i left campus?". Tapi aku dan teman-temanku masih berhubungan baik, ya setidaknya aku masih bergabung di group whatsapp kelas dan beberapa teman masih ada yang silaturahmi ke rumah. 

So, pesan nya apa?. Kerjakanlah sesuatu dengan kemampuan kita, bukan hanya hasil yang kita akan nikmati, tapi prosesnya juga perlu kita jadi sesuatu yang berharga untuk kita. Nilai A+ sekalipun kalau hasil mencontek pasti tidak akan membuat hati kita benar benar puas, lebih baik dapat nilai C dengan hasil perjuangan belajar sendiri. Itu dari saya, terimakasih sudah membaca. kalian luar biasa. 

